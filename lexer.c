/************************************************************************
University of Leeds
School of Computing
COMP2932- Compiler Design and Construction
Lexer Module

I confirm that the following code has been developed and written by me and it is entirely the result of my own work.
I also confirm that I have not copied any parts of this program from another person or any other source or facilitated someone to copy this program from me.
I confirm that I will not publish the program online or share it with anyone without permission of the module leader.

Student Name:
Student ID:
Email:
Date Work Commenced:
*************************************************************************/


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "lexer.h"


// YOU CAN ADD YOUR OWN FUNCTIONS, DECLARATIONS AND VARIABLES HERE

char a[22][50] = {"class", "constructor", "method","function","int","boolean"
        ,"char","void","var","static","field","let","do","if","else","while"
        ,"return","true","false","null","this"};

char legalSymbols[20] = {'(', ')', '[', ']', '{', '}', ',', ';',
                         '=', '.', '+', '-', '*', '/',
                         '&', '|', '~', '<', '>', '_'};


long number_of_symbols = 0;
char *file_information ;
char fileName[32];
char character;
int sizeOfToken = 0;
int triggerToGoBackToCleanWhiteSpacesAtTheEndOfTheComment = 0;
int lineNumber = 1;
int errorTrigger = 0;
int commentCorrector = 0;
FILE * fp = NULL;
Token t;


void lineNumberCounter()
{
    if(character == '\n'){
        lineNumber++;
        t.ln = lineNumber;
    }
}


int checkSymbolsIllegal()
{
    for (int i = 0; i < 20; ++i) {
        if(legalSymbols[i] == character){
            return 1;
        }
    }
    return 0;
}




int InitLexer (char* file_name)
{
    //open a file
    if((fp = fopen(file_name, "rb")) == NULL) {
        printf("file not found");
        return 0;

    } else {
        strncpy(fileName, file_name, 32);

        // find the number of bytes in the file to create an array
        // of proper size
//        fseek( fp , 0 , SEEK_END );
//        number_of_symbols = ftell(fp);
        rewind(fp);
    }
    //create a dynamic array and allocate its memory
    //file_information = (char *) calloc( 1, number_of_symbols);
    lineNumber = 1;
    errorTrigger = 0;
    return 1;
}



// Get the next token from the source file
Token GetNextToken ()
{

    char string_literal[50];
    string_literal[0] = '\0';
    for (int i = 1; i < 50-1; ++i) {
        string_literal[i] = '\n';
    }



    //Token t;
    while (1){
        triggerToGoBackToCleanWhiteSpacesAtTheEndOfTheComment = 0;
        commentCorrector = 0;

        //error exception
        if(errorTrigger == 1){ break; }



        //white spaces check
        character = fgetc(fp);
        lineNumberCounter(); //increment a line number
        while (character == ' ') {
            character = fgetc(fp);
            lineNumberCounter(); //increment a line number
        }


        // check for /* or /**
        if(character == '/') {
            character = fgetc(fp);
            lineNumberCounter(); //increment a line number

            if (character == '*') {

                commentCorrector = 1;

                character = fgetc(fp);
                lineNumberCounter(); //increment a line number

                if(character != '*') {

                    triggerToGoBackToCleanWhiteSpacesAtTheEndOfTheComment = 1;
                }
                if(triggerToGoBackToCleanWhiteSpacesAtTheEndOfTheComment == 1){

                    while (1){

                        if(character == '\r') {
                            character = fgetc(fp);
                            lineNumberCounter(); //increment a line number
                        }
                        if( character == '\n' || character == EOF ){





                            // EofInCom: End of file in comment
                            if(character == EOF){
                                t.tp = ERR;
                                t.ec = EofInCom;
                                char iligStringError[40] = "Error: unexpected eof in comment\0\0\0";
                                for (int i = 0; i < sizeof(iligStringError); ++i) {
                                    t.lx[i] = iligStringError[i];
                                }
                                errorTrigger = 1;
                                t.ln = lineNumber;





                                return t;

                            }





                            if( character == '\n' && commentCorrector == 1) {}else {

                                triggerToGoBackToCleanWhiteSpacesAtTheEndOfTheComment = 1;
                                break;
                            }

                        }

                        // check for closing a comment */ or // here removed // CHECK IT
                        character = fgetc(fp);
                        lineNumberCounter(); //increment a line number

                        if( character == '*'){
                            character = fgetc(fp);
                            lineNumberCounter(); //increment a line number

                            if(character  == '/'){
                                triggerToGoBackToCleanWhiteSpacesAtTheEndOfTheComment = 1;
                                break;
                            }
                        }
                    }
                }


                if(character == '*'){

                    while (character != EOF){


                        character = fgetc(fp);
                        lineNumberCounter(); //increment a line number

                        if(character == '*' ){
                            character = fgetc(fp);
                            lineNumberCounter(); //increment a line number

                            if(character  == '/'){
                                character = fgetc(fp);
                                lineNumberCounter(); //increment a line number
                                break;
                            }
                        }
                    }
                }

            } else {
                //back two characters
                fseek(fp, -2, SEEK_CUR);
                character = fgetc(fp);

            }
        }


        //comments check // or /*
        if(character == '/' ){

            character = fgetc(fp);

            if(character  == '/' || character == '*'){
                while (1){
                    if(character == '\r') {
                        character = fgetc(fp);
                        lineNumberCounter(); //increment a line number
                    }

                    // here was  "character == EOF"
                    if( character == '\n' ){








                        // changes // something I probably use as redundancy
                        if( character == '\n' || character == -1) {}
                        triggerToGoBackToCleanWhiteSpacesAtTheEndOfTheComment = 1;
                        break;
                    }




                    // EofInCom: End of file in comment
                    if(character == EOF){
                        t.tp = ERR;
                        t.ec = EofInCom;
                        char iligStringError[40] = "Error: unexpected eof in comment\0\0\0";
                        for (int i = 0; i < sizeof(iligStringError); ++i) {
                            t.lx[i] = iligStringError[i];
                        }
                        errorTrigger = 1;

                        t.ln = lineNumber;






                        return t;

                    }



                    // check for closing a comment */ or //
                    character = fgetc(fp);
                    lineNumberCounter(); //increment a line number
                    if( character == '*'){

                        character = fgetc(fp);
                        lineNumberCounter(); //increment a line number
                        if(character == '/') {
                            triggerToGoBackToCleanWhiteSpacesAtTheEndOfTheComment = 1;
                            break;
                        }
                    }
                }
            } else {
                //back one character
                fseek( fp , -2 , SEEK_CUR );
                character = fgetc(fp);


            }
        }


        if(character == EOF){
            t.tp = EOFile;

//            t.lx[0] = 'E';t.lx[1] = 'n';t.lx[2] = 'd'; t.lx[3] = ' ';
//            t.lx[4] = 'o';t.lx[5] = 'f';t.lx[6] = ' ';
//            t.lx[7] = 'F';t.lx[8] = 'i';t.lx[9] = 'l'; t.lx[10] = 'e'; t.lx[11] = '\0';

            char endFile[15] = "End of File\0\0\0";
            for (int i = 0; i < sizeof(endFile); ++i) {
                t.lx[i] = endFile[i];
            }

            t.ln = lineNumber;
            return t;
        }

        // check anything in comment ""
        if(character == '"'){
            int i = 0;
            character = fgetc(fp);
            lineNumberCounter(); //increment a line number
            while(character !='"'){

                // EofInStr: End of file in string literal
                if(character == EOF){

                    t.tp = ERR;
                    t.ec = EofInStr;
                    char iligStringError[50] = "Error: unexpected eof in string constant\0\0\0";
                    for (int i = 0; i < sizeof(iligStringError); ++i) {
                        t.lx[i] = iligStringError[i];
                    }
                    errorTrigger = 1;
                    t.ln = lineNumber;



                    printf("\n\n\n\n<%s, %d, %s, %u>\n\n\n\n", fileName, t.ln, t.lx, t.tp);


                    return t;

                }
                // NewLnInStr: New line in string literal
                if(character == '\n' || character == '\r'){
                    t.tp = ERR;
                    t.ec = NewLnInStr;
                    char iligStringError[40] = "Error: new line in string constant\0\0\0";
                    for (int i = 0; i < sizeof(iligStringError); ++i) {
                        t.lx[i] = iligStringError[i];
                    }
                    errorTrigger = 1;
                    t.ln = lineNumber;
                    return t;

                }


                string_literal[i] = character;
                character = fgetc(fp);
                lineNumberCounter(); //increment a line number
                i++;
            }
            string_literal[i] = '\0';
            //check and feel the token
            int o = 0;
            while (string_literal[o] != '\0'){
                t.lx[o] = string_literal[o];
                o++;
            }
            t.lx[o] = '\0';
            t.tp = STRING;
            t.ln = lineNumber;

            return t;

        }

        // check for alphabet symbol
        if(isalpha(character) || character =='_'){
            int i = 0;
            while(isalnum(character) || character == '_'){
                string_literal[i] = character;

                character = fgetc(fp);
                lineNumberCounter(); //increment a line number
                i++;
            }
            string_literal[i] = '\0';

            //check and feel the token
            int o = 0;
            while (string_literal[o] != '\0'){
                t.lx[o] = string_literal[o];
                o++;
            }
            t.lx[o] = '\0';



            //checking for reserved words
            for (int i = 0; i < 22; ++i) {
                if( strcmp(a[i], t.lx) == 0){
                    t.tp = RESWORD;



                    if(character == EOF){

                        //back one character
                        fseek( fp , -1 , SEEK_CUR );
                        character = fgetc(fp);
                        t.ln = lineNumber;
                    }
                    else {
                        //back one character
                        fseek( fp , -2 , SEEK_CUR );
                        character = fgetc(fp);
                        t.ln = lineNumber;

                    }


                    return t;
                }
            }
            t.tp = ID;





            //HERE
            if(character == EOF){

                //back one character
                fseek( fp , -1 , SEEK_CUR );
                character = fgetc(fp);
                t.ln = lineNumber;
                return t;}



            //back one character
            fseek( fp , -2 , SEEK_CUR );
            character = fgetc(fp);
            t.ln = lineNumber;

            return t;


        }

        //check for numbers
        if(isdigit(character)){
            int i = 0;
            while(isdigit(character)){
                string_literal[i] = character;
                character = fgetc(fp);
                lineNumberCounter(); //increment a line number
                i++;
            }

            string_literal[i] = '\0';

            //check and feel the token
            int o = 0;
            while (string_literal[o] != '\0'){
                t.lx[o] = string_literal[o];
                o++;
            }
            t.lx[o] = '\0';
            t.tp = INT;

            //back one character
            if(i == 1) {
                fseek( fp , -2 , SEEK_CUR );
                character = fgetc(fp);

            }else if(i > 1) {
                fseek( fp , -2 , SEEK_CUR );
                character = fgetc(fp);

            }
            t.ln = lineNumber;
            return t;

        } else if((triggerToGoBackToCleanWhiteSpacesAtTheEndOfTheComment == 0 && character != '\n')){

            int checkded = checkSymbolsIllegal();
            if(checkded == 1) {


                if(character == '\r'){
                    character = fgetc(fp);
                    lineNumberCounter(); //increment a line number
                }
                if(character != '\n' && character != '\t'){
                    t.lx[0] = character;
                    t.lx[1] = '\0';
                    t.tp = SYMBOL;
                    t.ln = lineNumber;
                    return t;
                }

            } else {


                if(character != '\r' && character != '\t'){

                    //Error illegal symbols in the file to find , which are not comented
                    t.tp = ERR;
                    t.ec = IllSym;
                    char iligStringError[40] = "Error: illegal symbol in source file\0\0\0";
                    for (int i = 0; i < sizeof(iligStringError); ++i) {
                        t.lx[i] = iligStringError[i];
                    }
                    errorTrigger = 1;
                    t.ln = lineNumber;
                    return t;

                }
            }
        }
    }
}



// peek (look) at the next token in the source file without removing it from the stream
Token PeekNextToken ()
{
    sizeOfToken = 0;


    if (character != EOF){
        GetNextToken();
    }

    for (int i = 0; t.lx[i] != '\0' ; ++i) {
        sizeOfToken++;
    }
    //go back by the number of characters in the token
    if(sizeOfToken > 2 && lineNumber != 1 && character != '"' && character != EOF){
        fseek(fp, -sizeOfToken-1, SEEK_CUR);
    } else if (sizeOfToken > 3 && character != EOF){
        fseek(fp, -sizeOfToken-3, SEEK_CUR);
    } else if( character != EOF){
        fseek(fp, -sizeOfToken-1, SEEK_CUR);
    }
    if (character != EOF){
        character = fgetc(fp);
    }



    return t;
}


// clean out at end, e.g. close files, free memory, ... etc
int StopLexer ()
{
    fclose(fp);
    //free(file_information);
    return 0;
}




// do not remove the next line
#ifndef TEST
int main ()
{
    // implement your main function here
    // NOTE: the autograder will not use your main function
    InitLexer("Main.jack");


    Token  p;


    p = GetNextToken();
    p = GetNextToken();
    p = GetNextToken();
    p = GetNextToken();
    p = GetNextToken();
    p = GetNextToken();
    p = GetNextToken();

//    while (character != EOF) {
//
//        printf("<%s, %d, %s, %u>\n", fileName, p.ln, p.lx, p.tp);
//        p = GetNextToken();
//    }

//    printf("<%s, %d, %s, %u>\n", fileName, p.ln, p.lx, p.tp);

    p = PeekNextToken();


    printf("<%s, %d, %s, %u>\n", fileName, p.ln, p.lx, p.tp);

    p = PeekNextToken();

    printf("<%s, %d, %s, %u>\n", fileName, p.ln, p.lx, p.tp);
    p = PeekNextToken();

    printf("<%s, %d, %s, %u>\n", fileName, p.ln, p.lx, p.tp);








    StopLexer();


    return 0;
}
// do not remove the next line
#endif
